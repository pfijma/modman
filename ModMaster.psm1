Get-ChildItem -Path $PSScriptRoot\private\*.ps1 -Exclude *.tests.ps1, *profile.ps1 |
ForEach-Object {
    . $_.FullName
}

Get-ChildItem -Path $PSScriptRoot\Public\*.ps1 -Exclude *.tests.ps1, *profile.ps1 |
ForEach-Object {   
    . $_.FullName  
	write-verbose $_.BaseName
    $OnlyShowIfVerbose = Export-ModuleMember -Function $_.BaseName 
    $OnlyShowIfVerbose | write-verbose 
}

