<#

    create a copy of a given user module
    add the contents from the old folder and update the manifest for new version

#>
function New-ModuleVersion{
    [CmdletBinding()]
    Param (
        # Define parameters below, each separated by a comma 
        [Parameter(Mandatory=$True)]
        [string]$Module,
        [Parameter(Mandatory=$false)]
        [boolean]$SkipLocationCheck = $false,
        [Parameter(Mandatory=$false)]
        [boolean]$LogToFile = $false,
    [string]$LogFileName = '.\result.log'
    )

  if($LogToFile){
    start-transcript $logfilename
  }

  #normally always check if it is a usermodule unless
  if (!($skiplocationcheck)){
    
    $mymods = ls (($env:PSModulePath).split(';') | where {$_ -match $env:USERNAME} )
    $mymods | write-debug

    if (!($mymods.name -contains $module)){
        write-warning 'not a user module - check documents\WindowsPowershell\Modules for valid modules to edit'
        return $mymods
    }
  }

  write-debug 'load modules'

  #$module = 'toolbox'
  $moduleName = $module 
  import-module $moduleName -force | out-null

  # Get-Module -Name $moduleName 
  # Get-Command -Module $moduleName 

  $lastModversion = (get-module -ListAvailable | sort name | get-unique |  where {$_.name -match $moduleName}) # | ft -HideTableHeaders
  # $lastModversion

  $manifest = Import-PowerShellDataFile $lastmodversion.Path
  # $manifest

  [version]$version = $Manifest.ModuleVersion
  [string]$OldVersion = "{0}.{1}.{2}" -f $Version.Major, $Version.Minor, $Version.Build
  [string]$NewVersion = "{0}.{1}.{2}" -f $Version.Major, $Version.Minor, ($Version.Build +1)

  $SrcPath =  (ls $lastmodversion.path).DirectoryName +'\' 
  $Dstpath = ((ls $lastmodversion.path).DirectoryName).Replace($oldversion,$newversion) + '\'

  ('{0} > {1}' -f $OldVersion,$NewVersion) | write-debug
  write-debug $SrcPath 
  write-debug $Dstpath

  if (test-path $Dstpath){
    #LS $Dstpath -Recurse
  }else{
    write-debug ('creating new version in folder: {0}' -f $Dstpath)
    mkdir $Dstpath | Out-Null
  }

  copy-item -Path ($SrcPath + '*') -recurse -Destination $Dstpath -Force | out-null

  $manifest = Import-PowerShellDataFile $lastmodversion.Path
  #$manifest.FunctionsToExport

  $newMod = ($Dstpath + $moduleName + '.psd1')
  New-ModuleManifest -Path $newMod  -ModuleVersion $NewVersion -FunctionsToExport '*' -RootModule $moduleName | out-null

  #check 
  # $manifest = Import-PowerShellDataFile $newmod
  # $manifest
  $filetodelete = ($Dstpath + 'public\get-version*') 
  $filetodelete | remove-item | out-null


  $nfn = @'
Function Get-Version {
    [CmdletBinding()]
    Param ()
    # origin: yyyy
    return $psScriptRoot
}
'@

  $NewOutputFileContent = $nfn.replace('zzzz',$NewVersion.replace('.','_')).Replace('qqqq',$moduleName).replace('yyyy',$Dstpath) 
  $NewOutputFileName    = ($Dstpath + 'public\' + "Get-Version_"+$modulename+'_'+$NewVersion.replace('.','_') + ".ps1")
  $NewOutputFileContent | out-file $NewOutputFileName

  write-debug $NewOutputFileName
  write-debug $NewOutputFileContent


  if($LogToFile){
    stop-transcript
  }

}


if ($false) {
  # create a new module based on the old module
  New-ModuleVersion -Module automaton
  # now you can edit the new version
}

if ($false) {
  # remove old version commands or removed commands from old module
  remove-module automaton -Force
  # load the new / latest version
  import-module automaton -force
  # check exisiting commands in module
  get-command -module automaton
}

