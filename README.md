# ModMan

Module  Manager

module manager is a tool to update my module structure and management files.  
Use this to create a new copy of the files that you can edit to make the new version.

it is based on a structure:  

My structure is relative to the location of all user modules:  
`c:\Users\<user>\Documents\WindowsPowerShell\Modules\`  
in this folder you create a folder:  
`<ModuleName>`  
(this will be the name for the module that you use to `Import-Module <modulename>` )  
  
i will use versioning to make new versions of the module:  
`<modulename>\<major.minor.build>\`  
This is for powershell to enable versioning.  
The following two files need to be added to the `<modulename>\<major.minorbuild>\` folder:  
`<modulename>.psd1`  
and  
`<modulename>.psm1`  

Add two subfolders:  
`\public` - used for code that is public visible ( `get-command <modulename>` )  
`\private`  - used for code that is NOT public visible (functions used by our own code).

The full path now looks like:
`c:\Users\<user>\Documents\WindowsPowerShell\Modules\<modulename>\<major.minor.build>\public\`

`C:\Users\pfijma\Documents\WindowsPowerShell\Modules\ModMaster\0.0.1\`

be aware: this module is currently very limited in its use, but can be used to understand versioning in powershell  

A module manifest is a PowerShell data file ( . psd1 ) that describes the contents of a module and determines how a module is processed. 
The manifest file is a text file that contains a hash table of keys and values

about psd1 files: [psd1](https://docs.microsoft.com/en-us/powershell/scripting/developer/module/how-to-write-a-powershell-module-manifest?view=powershell-6)
about psm1 files: [psm1](https://docs.microsoft.com/en-us/powershell/scripting/developer/module/writing-a-windows-powershell-module?view=powershell-6)
